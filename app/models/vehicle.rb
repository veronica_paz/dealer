class Vehicle < ActiveRecord::Base
  has_one :quote
  def details
    "#{model}, #{color}, #{vin}"
  end
end
