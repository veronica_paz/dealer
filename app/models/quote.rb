class Quote < ActiveRecord::Base
  has_many  :sales
  belongs_to :vehicle
  belongs_to :employee
  belongs_to :customer
  def taxes
    4.3
  end

  def markup
    10
  end
def total
  wholesale + (wholesale * markup/100) + (wholesale * taxes/100)

end
end
