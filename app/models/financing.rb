class Financing < ActiveRecord::Base

  def self.amortization_table(purchase_price = 0,loan_term = 0,interest_rate = 0)
    Rails.logger.debug "Inputs: #{purchase_price} #{loan_term} #{interest_rate}"

    financing = []
    fixed = purchase_price
    loan = loan_term*12

    (1..loan).each do |t|
      rate =(interest_rate/100)
      @@monthly_payment = (fixed*rate)/((1-(1+rate)**-loan))
      financing<< purchase_price- (@@monthly_payment -(purchase_price * rate))

      previous_amount = purchase_price - (@@monthly_payment - (purchase_price * rate))

      purchase_price = previous_amount

      @@interest_paid = (rate) * @@monthly_payment
      @@principal_paid = (@@monthly_payment-@@interest_paid)

       end
      return financing

  end

  def amortization_table
    return self.class.amortization_table(self.purchase_price,self.loan_term,self.interest_rate)
  end

def  self. monthly_payment
  @@monthly_payment
end
  def self.interest_paid
    @@interest_paid
  end
  def self.principal_paid
    @@principal_paid
  end

end
