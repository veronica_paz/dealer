# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

build_table = ->
  purchase_price = $("#finacing_purchase_price").val()
  loan_term =  $("#financing_loan_term").val()
  interest_rate = $("#financing_interest_rate").val()

  $.get "/financings/amortization",{purchase_price: purchase_price,loan_term: loan_term, interest_rate: interest_rate}

$ ->
  $('#financing_loan_term,#financing_interest_rate,#financing_purchase_price').bind 'keyup mouseup mousewheel', ->
    build_table()

  build_table()
