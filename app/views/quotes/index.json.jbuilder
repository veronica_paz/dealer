json.array!(@quotes) do |quote|
  json.extract! quote, :id, :wholesale, :markup, :taxes, :total, :status, :employee_id, :customer_id, :vehicle_id
  json.url quote_url(quote, format: :json)
end
