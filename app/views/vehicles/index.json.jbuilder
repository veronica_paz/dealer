json.array!(@vehicles) do |vehicle|
  json.extract! vehicle, :id, :vehicle_id, :customer_id, :year, :make, :model, :VIN, :color
  json.url vehicle_url(vehicle, format: :json)
end
