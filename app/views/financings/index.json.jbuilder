json.array!(@financings) do |financing|
  json.extract! financing, :id, :purchase_price, :loan_term, :interest_rate
  json.url financing_url(financing, format: :json)
end
