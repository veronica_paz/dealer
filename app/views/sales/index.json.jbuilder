json.array!(@sales) do |sale|
  json.extract! sale, :id, :markup, :tax
  json.url sale_url(sale, format: :json)
end
