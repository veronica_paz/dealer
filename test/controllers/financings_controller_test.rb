require 'test_helper'

class FinancingsControllerTest < ActionController::TestCase
  setup do
    @financing = financings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:financings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create financing" do
    assert_difference('Financing.count') do
      post :create, financing: { interest_rate: @financing.interest_rate, loan_term: @financing.loan_term, purchase_price: @financing.purchase_price }
    end

    assert_redirected_to financing_path(assigns(:financing))
  end

  test "should show financing" do
    get :show, id: @financing
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @financing
    assert_response :success
  end

  test "should update financing" do
    patch :update, id: @financing, financing: { interest_rate: @financing.interest_rate, loan_term: @financing.loan_term, purchase_price: @financing.purchase_price }
    assert_redirected_to financing_path(assigns(:financing))
  end

  test "should destroy financing" do
    assert_difference('Financing.count', -1) do
      delete :destroy, id: @financing
    end

    assert_redirected_to financings_path
  end
end
