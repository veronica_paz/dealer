class CreateSales < ActiveRecord::Migration
  def change
    create_table :sales do |t|
      t.float :markup
      t.float :tax

      t.timestamps null: false
    end
  end
end
