class CreateFinancings < ActiveRecord::Migration
  def change
    create_table :financings do |t|
      t.float :purchase_price
      t.float :loan_term
      t.float :interest_rate

      t.timestamps null: false
    end
  end
end
