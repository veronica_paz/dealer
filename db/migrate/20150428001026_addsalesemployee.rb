class addsalesemployee < activerecord::migration
  def change
    add_column :sales, :employee_id, :integer
    add_column :sales, :quote_id, :integer
  end
end
