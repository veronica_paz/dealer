class CreateVehicles < ActiveRecord::Migration
  def change
    create_table :vehicles do |t|
      t.integer :vehicle_id
      t.integer :customer_id
      t.string :year
      t.string :make
      t.string :model
      t.string :VIN
      t.string :color

      t.timestamps null: false
    end
  end
end
