class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.float :wholesale
      t.float :markup
      t.float :taxes
      t.float :total
      t.boolean :status
      t.string :employee_id
      t.string :customer_id
      t.string :vehicle_id

      t.timestamps null: false
    end
  end
end
